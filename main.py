"__authors__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment (unicode script fixes in place)
Boto docs: http://boto.cloudhackers.com
"""

import os
import io
import sys
import requests
import logging
import json
import gzip

import pip 
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'dateparser'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'pygelf'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'boto'])

from pygelf import GelfTcpHandler
import csv
import time
from keboola import docker
import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
import datetime
import dateparser
import pandas as pd

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    format="%(asctime)s: [%(levelname)s] - %(message)s",
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
KEY_ID = cfg.get_parameters()["keyId"]
SKEY_ID = cfg.get_parameters()["#secret_key"]
BUCKET = cfg.get_parameters()["bucket_source"]
ABBREV = cfg.get_parameters()["area"] #folder_string
FILENAME = cfg.get_parameters()["file_name"] #file_name
INCLUDE_FILE_NAME = cfg.get_parameters()["include_file_name"] #whether or not include the file name as a new column
START_DATE = cfg.get_parameters()["start_date"]
END_DATE = cfg.get_parameters()["end_date"]
COLUMN_HEADER = cfg.get_parameters()["column_header"]
INSERT_HEADER = cfg.get_parameters()["header_to_be_insert"]
INDEX_HEADER = cfg.get_parameters()["include_index"]

# Get proper list of tables
cfg = docker.Config('/data/')
out_tables = cfg.get_expected_output_tables()
logging.info("OUT tables mapped: "+str(out_tables))

DEFAULT_TABLE_INPUT = "/data/in/tables/"
DEFAULT_TABLE_OUTPUT = "/data/out/tables/"


def get_tables(out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.info("Output table: " + str(out_name))
    logging.info("Output table destination: " + str(out_Destination))

    return out_name


class sfile:
    """
    represents files on S3
    """

    def __init__(self, name, size, modified, key):
        self.name = name
        self.size = size
        self.modified = modified
        self.key = key


def dates_request(start_date, end_date):
    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            logging.error("ERROR: start_date cannot exceed end_date.")
            logging.error("Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        if day_diff == 0:
            ### normal with leading zeros
            dates.append(temp_date.strftime("%Y%m%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
        while day_n < day_diff:
            ### normal with leading zeros 
            dates.append(temp_date.strftime("%Y%m%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                ### normal with leading zeros
                dates.append(temp_date.strftime("%Y%m%d"))
                ### no leading zeros
                #dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        logging.error("ERROR: Please enter valid date parameters")
        logging.error("Exit.")
        sys.exit(1)

    return dates


def list_buckets():
    """
    List buckets and their properties
    """

    try:
        buckets = conn.get_all_buckets()
    except (OSError,S3ResponseError) as e:
        logging.error("Could not list buckets. Please check credentials. Exit!")
        sys.exit(0)

    buckets_names = []

    for i in buckets:
        name = i.name
        buckets_names.append(name)

    return buckets_names


def list_items(bucket_name, folder=""):
    """
    list items in the bucket.
    returns a list of objects
    
    supported info:
        .name
        .key
        .size
        .last_modified
    
    if nonexistent is None:
        print "No such bucket!"
    """

    # test if bucket is available
    """
    buckets_available = list_buckets()
    if bucket_name in buckets_available:
        logging.info("Bucket found on S3: "+bucket_name)
        pass
    else:
        logging.error("Bucket is not present on S3. Exit!")
        sys.exit(1)
    """
    bucket = conn.get_bucket(bucket_name, validate=False)
    items = []

    for i in bucket.list(prefix=folder):
        name = i.name
        location = i.key
        size = i.size
        modified = i.last_modified

        info = sfile(name, size, modified, location)
        items.append(info)

    return items


if __name__ == "__main__":
    """
    Main execution script.
    """
    bucket_name = BUCKET
    folder_string = ABBREV
    #file_name = FILENAME
    OUTPUT_FILE = get_tables(out_tables) 

    """ S3 connection test """
    try:
        conn = boto.connect_s3(KEY_ID,SKEY_ID)
        logging.info("Successfully connected."+str(conn))
    except Exception as a:
        logging.error("Could not connect. Exit!")
        sys.exit(1)
    #list_buckets()
    
    """ Generate a list of dates to download based on dates request """
    dates = dates_request(START_DATE,END_DATE)

    """ Find out which files need to be outputed """
    """ xxxx.NYYYYMMDD.gz & xxxx.NYYYYMMDDHHmmss.gz """
    itemslist = list_items(bucket_name,folder_string)
    full_list = []
    """
    for i in itemslist:
        for j in dates:
            string = FILENAME+".N"+str(j)
            if string in (i.name):
                full_list.append(i.name)
    """
    bad_dates = []
    for j in dates:
        string = FILENAME+".N"+str(j)
        bool_exist = False
        for i in itemslist:
            if string in (i.name):
                full_list.append(i.name)
                bool_exist = True
        if bool_exist==False:
            logging.info("{0} does not exist.".format(string))
            bad_dates.append(j)


    """ initialize the index column """
    if INDEX_HEADER:
        # if client want index-ed columns
        COLUMN_HEADER.append("index_col")


    """ Adding a "source file" column"""
    if INCLUDE_FILE_NAME:
        #if true, include source file column
        temp_list=["source_file"]
        for x in COLUMN_HEADER:
            temp_list.append(x)
        COLUMN_HEADER=temp_list


    """ Finding the columns which does not exist in the source file """
    """ Purpose: properly output the order of the columns """
    temp_column=[]
    for j in COLUMN_HEADER:
        temp_column.append(j)
    for j in INSERT_HEADER:
        # temp column header for the source file 
        # source file header length != desired output file header length
        n = temp_column.index(j)
        del temp_column[n]

    if INDEX_HEADER:
        n = temp_column.index('index_col')
        del temp_column[n]
    if INCLUDE_FILE_NAME:
        #temporary remove the "source_file" column to organize the source file column headers
        n = temp_column.index('source_file')
        del temp_column[n]

    """ Output  by files """
    output_df = pd.DataFrame(columns=COLUMN_HEADER)
    #for i in dates:
    good_output = []
    bad_output = []
    for i in full_list:
        #file_source = folder_string+"/"+file_name+".N"+i+".gz"
        file_name = i.split("/")[3]
        file_name = file_name.split(".gz")[0]
        file_source = i
        logging.info("file source: {0}".format(file_source))
        file_input = DEFAULT_TABLE_INPUT+file_name+".gz"
        logging.info("file_output destination: {0}".format(file_input))
        
        """ connecting to s3 """
        """ output the file into the input mapping """
        bucket = conn.get_bucket(bucket_name,validate=False)
        k = Key(bucket,file_source)
        try:
            k.get_contents_to_filename(file_input)
            
            """ Reading the source file as DF"""
            logging.info(file_source+" is downloaded.")
            #data = pd.read_table(gzip.open(file_input),dtype=str,sep=',',skiprows=1,header=None,encoding='ISO-8859-1')
            data = pd.read_table(file_input,compression='gzip',dtype=str,sep=',',skiprows=1,header=None,encoding='ISO-8859-1')

            """ Terminate if column headers do not match """
            if (len(data.columns) != len(temp_column)):
                logging.error("ERROR: Mismatch column headers ( Input: {0}, Source file: {1} )".format(len(temp_column),len(data.columns)))
                sys.exit(0)

            logging.info("Number of rows: {0}".format(str(len(data))))

            """ Assigning column headers"""
            data.columns = temp_column
            data = data[:len(data)-1]
            
            if INDEX_HEADER:
                # if client want index-ed columns
                data['index_col']=data.index
            
            if INCLUDE_FILE_NAME:
                # assigning values for source_file column
                data['source_file']=file_name+".gz"

            for j in INSERT_HEADER:
                # Assigning values for newly inserted column
                data[j]=0

            """ Output """
            file_output = file_name+".csv"
            logging.info("Output: "+DEFAULT_TABLE_OUTPUT+file_output)
            #data_df = pd.DataFrame(data)
            #data.to_csv(DEFAULT_TABLE_OUTPUT+file_output,index=False,columns=COLUMN_HEADER)

            #output_df = output_df.append(data)
            """ Append the modified data to the output table """
            with open(OUTPUT_FILE,'a') as f:
                data.to_csv(f,index=False,columns=COLUMN_HEADER,compression='gzip')
            f.close()
            del data
            good_output.append(i)
            
        except Exception as b:
            logging.error("ERROR: {0} - {1}".format(i,b))
            bad_output.append(i)
    
    """ Output """
    ### list of files accepted
    logging.info("INFO: Concatenated Outputs: {0}".format(good_output))
    ### list of files having problems
    logging.info("INFO: Rejected Files: {0}".format(bad_output))
    ### list of files does not exist within the given date range
    logging.info("INFO: Does not exist list: {0}".format(bad_dates))
        
    #output_df.to_csv(OUTPUT_FILE,index=False,columns=COLUMN_HEADER)
    
    logging.info("Done.")
    